import React from "react";
import Image from "../Image/waroeng.jpeg";
import "../style/footer.css";

export default function Footer() {
  return (
    <div>
      <div className="footer">
        <div className="block">
          <img className="fot" src={Image} alt="" />
          <br />
          <h5 className="inf"># NYAMAN & BANAR</h5>
          <ul className="icon ">
          <a className="navbar-brand btn" href="https://www.facebook.com/steaknyaindonesia/"><i class="fa-brands fa-facebook"></i></a>
          <a className="navbar-brand btn" href="https://www.instagram.com/waroengsteak/"><i class="fa-brands fa-instagram"></i></a>
          <a className="navbar-brand btn" href="https://twitter.com/waroengsteak"><i class="fa-brands fa-twitter"></i></a>
          <a className="navbar-brand btn" href="https://www.tiktok.com/@waroengsteak"><i class="fa-brands fa-tiktok"></i></a>
          <a className="navbar-brand btn" href="https://www.youtube.com/c/WaroengSteakIndonesia"><i class="fa-brands fa-youtube"></i></a>
          <a className="navbar-brand btn" href="https://web.whatsapp.com/"><i class="fa-brands fa-whatsapp"></i></a>
          </ul>
        </div>
        {/* <div className="inf">
          <h4>Informasi</h4>
          <p>Profil Waroeng Steak & Shake</p>
          <p>Sertifikasi dan</p>
          <p>Jaminan Kualitas</p>
        </div>

        <div className="inf">
          <h4 style={{marginBottom:"10px"}}>Layanan</h4>
          <div style={{marginBottom:"60px"}}>
          <p>Waroeng Food Truck</p>
          <p>Saran & Kritik</p>
          </div>
        </div> */}

        <div className="phg">
          <h4 className="pp">Penghargaan Dan Sertifikat</h4>
          <img
            className="fott"  
            src="https://waroengsteakandshake.com/assets/v2/img/top-digital-pr-2022.png"
            alt=""
          />
          <img
            className="fott"
            src="https://waroengsteakandshake.com/assets/v2/img/LOGO%20MURI.png"
            alt=""
          />
          <img
            className="fott"
            src="https://waroengsteakandshake.com/assets/v2/img/logo-halal-2.png"
            alt=""
          />
          <img
            className="fott"
            src="https://waroengsteakandshake.com/assets/v2/img/logo-top-brand-2022.png"
            alt=""
          />
        </div>
      </div >
      <div  style={{height:"20px"}}>
        <p className="rigt">Hak Cipta © 2020 Waroeng Kualitas.</p>
        </div>
    </div>
  );
}
