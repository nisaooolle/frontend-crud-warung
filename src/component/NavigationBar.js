import axios from "axios";
import React, { useState } from "react";
import { Form, InputGroup, Modal } from "react-bootstrap";
import { useHistory } from "react-router-dom";
import Swal from "sweetalert2";
import Image from "../Image/waroeng.jpeg";
import "../style/nav.css";
export default function NavigationBar() {
  const [show, setShow] = useState(false);
  const [image, setImage] = useState("");
  const [nama, setNama] = useState("");
  const [deskripsi, setDeskripsi] = useState("");
  const [harga, setHarga] = useState("");
  const history = useHistory();
  const handleClose = () => setShow(false); // fungsi handleClose akan menyetel variabel acara ke false.
  const handleShow = () => setShow(true); //  menyetel variabel status acara ke true,

  // const addUser = async (e) => {
  //   e.preventDefault();

  // const data = {
  //   link: link,
  //   namamakanan: namamakanan,
  //   deskripsi: deskripsi,
  //   harga: harga,
  // };

  //   //axios post untuk menambahkan data ke database
  //   await axios.post("http://localhost:8000/makanans", data);
  //   Swal.fire({
  //     icon: "success",
  //     title: "Your work has been saved",
  //     showConfirmButton: false,
  //     timer: 1500,
  //   })
  //     .then(() => {
  //       window.location.reload(); //otomatis reload web setelah menambahkan data
  //     })
  //     .catch((error) => {
  //       // alert error untuk mengetahui jika terjadi error
  //       alert("Terjadi kesalahan" + error);
  //     });
  // };

  const addUser = async (e) => {
    e.preventDefault();
    e.persist();

    const formData = new FormData();
    formData.append("file", image);
    formData.append("nama", nama);
    formData.append("deskripsi", deskripsi);
    formData.append("harga", harga);

    // try catch untuk memastikan trjdi kesalahan
    try {
      // library opensource yg digunkan untuk request data melalui http
      await axios.post(
        "http://localhost:5000/product",
        formData,
        // headers berikut berfungs untuk method yg hanya diakses oleh admin
        {
          headers: {
            Authorization: `Bearer ${localStorage.getItem("token")}`,
            "Content-Type": "multipart/form-data"
          },
        }
      );
      // Sweet alert
      setShow(false);
      Swal.fire({
        icon: "success",
        title: "Berhasil menambahkan data!!",
        showConfirmButton: false,
        timer: 1500,
      });
      setTimeout(() => {
        window.location.reload();
      }, 1500);
    } catch (error) {
      console.log(error);
    }
  };
  const logout = () => {
    window.location.reload();
    localStorage.clear();
    history.push("/login");
  };
  return (
    <div className="nav">
      <nav class="navbar navbar-expand-lg">
        <div class="container-fluid bar">
          <a className="navbar-brand" href="#">
            <img className="images" src={Image} alt="" />
          </a>
          <button
            class="navbar-toggler"
            type="button"
            data-bs-toggle="collapse"
            data-bs-target="#navbarSupportedContent"
            aria-controls="navbarSupportedContent"
            aria-expanded="false"
            aria-label="Toggle navigation"
          >
            <span class="navbar-toggler-icon"></span>
          </button>
          <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav me-auto mb-2 mb-lg-0">
              <li class="nav-item">
                <a class="nav-link btn" aria-current="page" href="/">
                  Home
                </a>
              </li>
              {localStorage.getItem("role") === "ADMIN" ? (
                <>
                  <li className="nav-item">
                    <a className="nav-link btn" onClick={handleShow}>
                      Tambah Product
                    </a>
                  </li>
                </>
              ) : (
                <></>
              )}
               {localStorage.getItem("id") !== null ? (
               <li className="nav-item float-ringht">
                  <a className="nav-link btn" href="/profil">
                    Profile
                  </a>
                </li>
               ) : (
                <></>
               )}
               {localStorage.getItem("id") !== null ? (
                <li class="nav-item crt">
                <a class="nav-link" href="/cart">
                  <i class="fa-solid fa-cart-plus"></i>
                </a>
              </li>
               ) : (
                <></>
               )}
              {localStorage.getItem("id") !== null ? (
                <li className="nav-item float-ringht lo">
                  <a className="nav-link btn" onClick={logout}><i class="fa-solid fa-right-from-bracket"></i>
                  </a>
                </li>
              ) : (
                <li className="nav-item loo">
                  <a className="nav-link" href="/login"><i class="fa-solid fa-right-to-bracket"></i>
                  </a>
                </li>
              )}
             
              {/* <li class="nav-item dropdown">
                <a
                  class="nav-link dropdown-toggle"
                  href="#"
                  role="button"
                  data-bs-toggle="dropdown"
                  aria-expanded="false"
                >
                  List Menu
                </a>
                <ul class="dropdown-menu">
                  <li>
                    <a class="dropdown-item" href="/makanan">
                      Makanan
                    </a>
                  </li>
                  <li>
                    <a class="dropdown-item" href="/minuman">
                      Minuman
                    </a>
                  </li>
                  <li>
                    <hr class="dropdown-divider" />
                  </li>
                  <li>
                    <a class="dropdown-item" href="/camilan">
                      {" "}
                      Camilan
                    </a>
                  </li>
                </ul>
              </li> */}
              
            </ul>
          </div>
        </div>
      </nav>
      {/* modal untuk mengisi data saat ingin menambahkan isi table */}
      <Modal show={show} onHide={handleClose}>
        <Modal.Header
          className="add"
          closeButton
          style={{ backgroundColor: "#2B3A55", color: "white" }}
        >
          <Modal.Title>Tambah menu</Modal.Title>
        </Modal.Header>
        <Modal.Body style={{ backgroundColor: "#FFF8BC" }}>
          <Form onSubmit={addUser}>
            <div className="mb-3">
              <Form.Label>
                <strong>Image</strong>
              </Form.Label>
              <InputGroup className="d-flex gap-3">
                <Form.Control
                  placeholder="Masukkan Link"
                  // value={image}
                  type="file"
                  onChange={(e) => setImage(e.target.files[0])}
                  required
                />
              </InputGroup>
            </div>
            <div className="mb-3">
              <Form.Label>
                <strong>Nama Product</strong>
              </Form.Label>
              <InputGroup className="d-flex gap-3">
                <Form.Control
                  placeholder="Masukkan nama product"
                  value={nama}
                  onChange={(e) => setNama(e.target.value)}
                  required
                />
              </InputGroup>
            </div>
            <div className="mb-3">
              <Form.Label>
                <strong>Deskripsi</strong>
              </Form.Label>
              <InputGroup className="d-flex gap-3">
                <Form.Control
                  type="text"
                  placeholder="Masukkan Deskripsi"
                  value={deskripsi}
                  onChange={(e) => setDeskripsi(e.target.value)}
                  required
                />
              </InputGroup>
            </div>
            <div className="mb-3">
              <Form.Label>
                <strong>Harga</strong>
              </Form.Label>
              <InputGroup className="d-flex gap-3">
                <Form.Control
                  placeholder="Masukkan Harga"
                  value={harga}
                  type={Number}
                  onChange={(e) => setHarga(e.target.value)}
                  required
                />
              </InputGroup>
            </div>
            <button
              className="mx-1 button-btl btn btn-danger"
              onClick={handleClose}
            >
              Close
            </button>
            <button
              type="submit"
              className="mx-1 button btn btn-primary"
              onClick={handleClose}
            >
              Save
            </button>
          </Form>
        </Modal.Body>
      </Modal>
    </div>
  );
}
