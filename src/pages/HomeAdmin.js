import axios from "axios";
import React, { useEffect, useState } from "react";
import { Button } from "react-bootstrap";
import Swal from "sweetalert2";
import "../style/homeAdmin.css";
import ReactPaginate from "react-paginate";

export default function HomeAdmin() {
  const [menu, setMenu] = useState([]); //useState berfungsi untuk menyimpan data sementara
  const [pages, setPages] = useState(0);
  const [nama, setNama] = useState("");

  //untuk melihat semua data
  const getAll = async (idx) => {
    await axios
      .get(`http://localhost:5000/product/all?nama=${nama}&page=${idx}`)
      .then((res) => {
        setMenu(res.data.data.content);
        setPages(res.data.data.totalPages);
      })
      .catch((error) => {
        alert("Terjadi kesalahan" + error);
      });
  };

  useEffect(() => {
    //mengambil data, memperbarui DOM secara langsung,
    getAll(0);
  }, []);

  //menghapus data
  const deleteUser = async (id) => {
    Swal.fire({
      title: "Yakin ingin menghapus data ini?",
      text: "Data kamu tidak akan bisa dikembalikan!",
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Yes, delete it!",
    }).then((result) => {
      if (result.isConfirmed) {
        axios.delete("http://localhost:5000/product/" + id);
        Swal.fire("Deleted!", "Your file has been deleted.", "success");
        setTimeout(() => {
          // window.location.reload();
        }, 1000);
      }
    }); // untuk pemberitahuan jika sudah berhasil menghapus
    getAll(0);
  };

  // totalHarga() {
  //   return this.cart.reduce((a, b) => a + b.harga, 0);
  // },
  return (
    <div className="isi">
    <div >
    <div className=" my-5 responsive-3">
      <form class="d-flex" role="search">
        <input
          class="form-control me-2"
          type="search"
          placeholder="Search"
          aria-label="Search"
          value={nama}
          onChange={(e) => setNama(e.target.value)}
        />
        <button
          class="btn btn-outline-success"
          type="button"
          onClick={() => getAll(0)}
        >
          Search
        </button>
      </form>
      <br />
      <table className="table table-bordered">
        <thead>
          <tr className="tabel">
            <th>No</th>
            <th className="img">Image</th>
            <th className="nama">Nama Product</th>
            <th className="des">Deskripsi</th>
            <th className="harga">Harga</th>
            {localStorage.getItem("id") !== null ? <th>action</th> : <></>}
          </tr>
        </thead>
        <tbody className="map">
          {menu.map(
            (
              makan,
              index //map untuk memetakan data
            ) => (
              <tr key={makan.id}>
                <td>{index + 1}</td>
                <td>
                  <img src={makan.image} alt="" />
                </td>
                <td>{makan.nama}</td>
                <td>{makan.deskripsi}</td>
                <td>{makan.harga}</td>
                {localStorage.getItem("id") !== null ? (
                  <td className="data">
                    <Button //button klik untuk delete
                      variant="danger"
                      className="mx-1"
                      onClick={() => deleteUser(makan.id)}
                    >
                      Hapus
                    </Button>
                    {/* untuk mengarahkan web ke path edit */}
                    <a href={"/edit/" + makan.id}>
                      <Button //button klik untuk edit
                        variant="warning"
                        className="mx-1"
                      >
                        Ubah
                      </Button>
                    </a>
                  </td>
                ) : (
                  <></>
                )}
              </tr>
            )
          )}
        </tbody>
        <br />
      </table>
      {menu.length == 0 ? (
        <p align="center">Product tidak tersedia</p>
      ) : (
        <div>
          <ReactPaginate
            previousLabel={"previous"}
            nextLabel={"next"}
            breakLabel={"..."}
            pageCount={pages}
            marginPagesDisplayed={2}
            pageRangeDisplayed={2}
            onPageChange={(e) => getAll(e.selected)}
            containerClassName={"pagination d-flex justify-content-center"}
            pageClassName={"page-item"}
            pageLinkClassName={"page-link"}
            previousClassName={"page-item"}
            previousLinkClassName={"page-link"}
            nextClassName={"page-item"}
            nextLinkClassName={"page-link"}
            breakClassName={"page-item"}
            breakLinkClassName={"page-link"}
            activeClassName={"active"}
          />
        </div>
      )}
    </div>
    </div>
    </div>
  );
}
