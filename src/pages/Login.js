import axios from "axios";
import React, { useState } from "react";
import { Form, InputGroup } from "react-bootstrap";
import { useHistory } from "react-router-dom";
import Swal from "sweetalert2";
import "../style/login.css";

const Login = () => {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");

  const history = useHistory();

  // const login = async (e) => {
  //   e.preventDefault();
  //   axios.get(" http://localhost:8000/users").then(({ data }) => {
  //     const user = data.find(
  //       (x) => x.email === email && x.password === password
  //     );
  //     if (user) {
  //       Swal.fire({
  //         icon: "success",
  //         title: "Selamat Datang!",
  //         showConfirmButton: false,
  //         timer: 1500,
  //       });
  //       localStorage.setItem("id", user.id);
  //       history.push("/");
  //       setTimeout(() => {
  //         window.location.reload();
  //       }, 1000);
  //     } else {
  //       Swal.fire({
  //         icon: "error",
  //         title: "Email Or Password Not Found",
  //         showConfirmButton: false,
  //         timer: 1500,
  //       });
  //     }
  //   });
  // };

  const login = async (e) => {
    e.preventDefault();

    try {
      const { data, status } = await axios.post(
        "http://localhost:5000/user/sign-in",
        {
          email: email,
          password: password,
        }
      );
      // jika respon status 200 / ok
      if (status === 200) {
        Swal.fire({
          icon: "success",
          title: "Login Berhasill!",
          showConfirmButton: false,
          timer: 1500,
        });
        localStorage.setItem("id", data.data.user.id);
        localStorage.setItem("token", data.data.token);
        localStorage.setItem("role", data.data.user.role);
        console.log(data);
        history.push("/");
      }
    } catch (error) {
      Swal.fire({
        icon: "error",
        title: "Username atau password tidak valid!",
        showConfirmButton: false,
        timer: 1500,
      });
      console.log(error);
    }
  };
  return (
    <div className="container border my-5 pt-3 pb-5 px-5">
      <div className="sr">
    <img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRw5JMEK5hZplsAbOG4E17xK5AeYlI1op7z9JVgBC7lM3lgnwSzk8aHdu4JN7WmMzVR29c&usqp=CAU" alt="" />
    </div>
      <h2 className="mb-5">Selamat datang di warung online!Silahkan login</h2>
      <Form onSubmit={login} method="POST">
        <div className="mb-3">
          <Form.Label>
            <strong>Email</strong>
          </Form.Label>
          <InputGroup className="d-flex gap 3">
            <Form.Control
              placeholder="Masukkan Email"
              type="email"
              value={email}
              onChange={(e) => setEmail(e.target.value)}
              required
            />
          </InputGroup>
        </div>
        <div className="mb-3">
          <Form.Label>
            <strong>Password</strong>
          </Form.Label>
          <InputGroup className="d-flex gap 3">
            <Form.Control
              placeholder="Password"
              type="password"
              value={password}
              onChange={(e) => setPassword(e.target.value)}
              required
            />
          </InputGroup>
        </div>
        <button type="submit" className="mx-1  btn btn-primary">
          Login
        </button>
      </Form>
      <br />
      <p>
        <h2 className="notReg">
          Silahkan register Jika Tidak Memiliki Akun
        </h2>
        <a href="/register" className="re btn">
          {" "}
          Register
        </a>
      </p>
      {/* <p>
        <span className="notReg">
          Masuk sebagai Admin
        </span> 
        <a href="/admin" className="btn btn-warning">
          {" "}
          Login Admin
        </a>
      </p> */}
    </div>
  );
};

export default Login;
