// import axios from "axios";
// import React, { useEffect, useState } from "react";
// import { useHistory } from "react-router-dom";
// import Swal from "sweetalert2";
// import "../style/makanan.css"

// export default function Minuman() {
//   const [menu, setMenu] = useState([]); //useState berfungsi untuk menyimpan data sementara

//   const history = useHistory();

//   //untuk melihat semua data
//   // const getAll = () => {
//   //   axios
//   //     .get("http://localhost:5000/product")
//   //     .then((res) => {
//   //       setMenu(res.data);
//   //     })
//   //     .catch((error) => {
//   //       alert("Terjadi kesalahan" + error);
//   //     });
//   // };

//   const getAll = async () => {
//     await axios
//       .get("http://localhost:5000/product/all")
//       .then((res) => {
//         setMenu(res.data.data);
//       })
//       .catch((error) => {
//         alert("Terjadi kesalahan" + error);
//       });
//   };

//   const addToCart = async (makanan) => {
//     await axios
//     .post("http://localhost:5000/cart/", makanan)
//     .then(() => {
//       Swal.fire({
//         icon: "success",
//         title: "berhasil masuk ke keranjang",
//         showConfirmButton: false,
//         timer: 1500,
//       });
//       history.push("/cart")
//     })
//     .catch((error) => {
//       alert("Terjadi kesalahan" + error);
//     });
// };

//   useEffect(() => {
//     //mengambil data, memperbarui DOM secara langsung,
//     getAll();
//   }, []);
//   return (
//     <div className="mkn">
//     <div className="cr flex-wrap">
//       {menu.map((makanan) => (
//         <div
//           data-aos="zoom-in"
//           className="card mb-3"
//           style={{ width: "18rem" }}
//         >
//           <img src={makanan.image} className="card-img-top" alt="..." />
//           <div className="card-body">
//             <h5 className="card-title">{makanan.nama}</h5>
//             <p className="card-text">{makanan.deskripsi}</p>
//             <p>Rp.{makanan.harga}</p>
//             {localStorage.getItem("id") !== null ? (
//               <a
//                 onClick={() => addToCart(makanan)}
//                 className="btn btn-warning"
//               >
//                 Beli
//               </a>
//             ) : (
//               <></>
//             )}
//           </div>
//         </div>
//       ))}
//     </div>
//     </div>
//   );
// }

// {/* <button id="button" class="show"></button> */}