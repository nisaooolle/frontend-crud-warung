import axios from "axios";
import React, { useEffect, useState } from "react";
import { Button } from "react-bootstrap";
import { useHistory } from "react-router-dom";
import Swal from "sweetalert2";
import "../style/home.css";
import ReactPaginate from "react-paginate";
import Footer from "../component/Footer";

export default function Home() {
  const [menu, setMenu] = useState([]); //useState berfungsi untuk menyimpan data sementara
  const [pages, setPages] = useState(0);

  const history = useHistory();

  //untuk melihat semua data
  // const getAll = () => {
  //   axios
  //     .get("http://localhost:5000/product")
  //     .then((res) => {
  //       setMenu(res.data);
  //     })
  //     .catch((error) => {
  //       alert("Terjadi kesalahan" + error);
  //     });
  // };

  const getAll = async (idx) => {
    await axios
      .get("http://localhost:5000/product/all?page=" + idx)
      .then((res) => {
        setMenu(
          res.data.data.content.map((item) => ({
            ...item,
            quantity: 1,
          }))
        );
        setPages(res.data.data.totalPages);
      })
      .catch((error) => {
        alert("Terjadi kesalahan" + error);
      });
  };

  const plusQuantity = (idx) => {
    const product = [...menu];
    if (product[idx].quantity <= 44) {
      product[idx].quantity++;
      setMenu(product);
    }
  };

  const minesQuantity = (idx) => {
    const product = [...menu];
    if (product[idx].quantity > 1) {
      product[idx].quantity--;
      setMenu(product);
    }
  };
  // const addToCart = async (makan) => {
  //   await axios
  //     .post("http://localhost:5000/cart/", makan)
  //     .then(() => {
  //       Swal.fire({
  //         icon: "success",
  //         title: "berhasil masuk ke keranjang",
  //         showConfirmButton: false,
  //         timer: 1500,
  //       });
  //       history.push("/");
  //       window.location.reload();
  //     })
  //     .catch((error) => {
  //       alert("Terjadi kesalahan" + error);
  //     });
  // };

  const addToCart = (makan) => {
    console.log(makan);
    axios
      .post("http://localhost:5000/cart/", {
        loginUserId: localStorage.getItem("id"),
        productId: makan.id,
        quantity: makan.quantity,
      })
      .then(() => {
        Swal.fire({
          icon: "success",
          title: "berhasil masuk ke keranjang",
          showConfirmButton: false,
          timer: 1500,
        });
      })
      .catch((error) => {
        alert("Terjadi kesalahan" + error);
      });
  };

  useEffect(() => {
    //mengambil data, memperbarui DOM secara langsung,
    getAll(0);
  }, []);
  return (
    <div className="isi">
      <div
        id="carouselExampleIndicators"
        className="carousel slide silde1"
        data-bs-ride="true"
      >
        <div class="carousel-indicators">
          <button
            type="button"
            data-bs-target="#carouselExampleIndicators"
            data-bs-slide-to="0"
            class="active"
            aria-current="true"
            aria-label="Slide 1"
          ></button>
          <button
            type="button"
            data-bs-target="#carouselExampleIndicators"
            data-bs-slide-to="1"
            aria-label="Slide 2"
          ></button>
          <button
            type="button"
            data-bs-target="#carouselExampleIndicators"
            data-bs-slide-to="2"
            aria-label="Slide 3"
          ></button>
        </div>
        <div class="carousel-inner">
          <div class="carousel-item active">
            <img
              src="https://waroengsteakandshake.com/uploads/2023/01/09/WEB_BANNER_PAKET_HEMAT_WEEKEND_SERU.jpg"
              class="d-block w-100"
              alt="..."
            />
          </div>
          <div class="carousel-item">
            <img
              src="https://storage.googleapis.com/fastwork-static/85d992ac-809f-4364-9930-2a1137d3c44b.jpg"
              class="d-block w-100"
              alt="..."
            />
          </div>
          <div class="carousel-item">
            <img
              src="https://waroengsteakandshake.com/uploads/2022/10/12/PROMO_LIVIN_THE_COFFEE.jpg"
              class="d-block w-100"
              alt="..."
            />
          </div>
        </div>
        <button
          class="carousel-control-prev"
          type="button"
          data-bs-target="#carouselExampleIndicators"
          data-bs-slide="prev"
        >
          <span class="carousel-control-prev-icon" aria-hidden="true"></span>
          <span class="visually-hidden">Previous</span>
        </button>
        <button
          class="carousel-control-next"
          type="button"
          data-bs-target="#carouselExampleIndicators"
          data-bs-slide="next"
        >
          <span class="carousel-control-next-icon" aria-hidden="true"></span>
          <span class="visually-hidden">Next</span>
        </button>
      </div>
      <hr style={{height:5,backgroundColor:"#B7B78A"}} />
      <h2 className="baru">Daftar Menu</h2>
      <hr style={{height:5,backgroundColor:"#B7B78A"}} />
      <div className="cr flex-wrap">
        {menu.map((makan, idx) => (
          <div
            data-aos="zoom-in"
            className="card mb-3"
            style={{ width: "18rem" }}
          >
            <img
              src={makan.image}
              className="card-img-top crr"
              width={50}
              height={200}
              alt="..."
            />
            <div className="card-body">
              <h5 className="card-title">{makan.nama}</h5>
              <p className="card-text">{makan.deskripsi}</p>
              <p>
                {" "}
                {makan.quantity == 1 ? (
                  <button
                    className="qty gray"
                    onClick={() => minesQuantity(idx)}
                  >
                    {" "}
                    -{" "}
                  </button>
                ) : (
                  <button
                    className="qty green"
                    onClick={() => minesQuantity(idx)}
                  >
                    {" "}
                    -{" "}
                  </button>
                )}
                <button className="ss">{makan.quantity}</button>
                {makan.quantity <= 44 ? (
                  <button
                    className="qty green"
                    onClick={() => plusQuantity(idx)}
                  >
                    +
                  </button>
                ) : (
                  <button
                    className="qty gray"
                    onClick={() => plusQuantity(idx)}
                  >
                    +
                  </button>
                )}
              </p>
              <p>Rp.{makan.harga}</p>
              {localStorage.getItem("id") !== null ? (
                <Button
                  onClick={() => addToCart(makan)}
                  className="beli btn btn-warning"
                >
                  Beli
                </Button>
              ) : (
                <></>
              )}
            </div>
          </div>
        ))}
      </div>
      <div>
        <ReactPaginate
          previousLabel={"previous"}
          nextLabel={"next"}
          breakLabel={"..."}
          pageCount={pages}
          marginPagesDisplayed={2}
          pageRangeDisplayed={2}
          onPageChange={(e) => getAll(e.selected)}
          containerClassName={"pagination d-flex justify-content-center"}
          pageClassName={"page-item"}
          pageLinkClassName={"page-link"}
          previousClassName={"page-item"}
          previousLinkClassName={"page-link"}
          nextClassName={"page-item"}
          nextLinkClassName={"page-link"}
          breakClassName={"page-item"}
          breakLinkClassName={"page-link"}
          activeClassName={"active"}
        />
      </div>
      <Footer />
    </div>
  );
}
