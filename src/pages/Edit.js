import axios from "axios";
import React, { useEffect, useState } from "react";
import { Form, InputGroup } from "react-bootstrap";
import { useHistory, useParams } from "react-router-dom";
// import "../style/edit.css";
import Swal from "sweetalert2";

const Edit = () => {
  //method edit
  const param = useParams(); //mengembalikan objek
  const [image, setImage] = useState("");
  const [nama, setNama] = useState("");
  const [deskripsi, setDeskripsi] = useState("");
  const [harga, setHarga] = useState("");

  const history = useHistory(); // akses ke instance riwayat yang dapat digunakan untuk bernavigasi.

  useEffect(() => {
    axios
      .get("http://localhost:5000/product/" + param.id)
      .then((response) => {
        const newFood = response.data.data;
        setImage(newFood.image);
        setNama(newFood.nama);
        setDeskripsi(newFood.deskripsi);
        setHarga(newFood.harga);
      })
      .catch((error) => {
        alert("Terjadi Kesalahan " + error);
      });
  }, []);

  const submitActionHandler = async (e) => {
    e.preventDefault();

    const formData = new FormData();
    formData.append("file", image);
    formData.append("nama", nama);
    formData.append("deskripsi", deskripsi);
    formData.append("harga", harga);

    Swal.fire({
      title: "want to edit?",
      text: "You clicked the button!",
      icon: "question",
      showCancelButton: true,
      confirmButtonColor: "#5F8D4E",
      cancelButtonColor: "#d33",
      confirmButtonText: "Yes, edit it!",
    }).then((result) => {
      if (result.isConfirmed) {
      axios
          .put(
            "http://localhost:5000/product/" + param.id, formData,
            // {
            //   headers: {
            //     "Content-Type": "multipart/form-data"
            //   },
            // }
          )
          .then(() => {
            history.push("/homeAdmin");
            Swal.fire("Berhasil", "Data kamu berhasil di edit");
          })
          .catch((error) => {
            console.log(error);
          });
      }
    });
  };

  return (
    <div className="edit mx-5">
      <div className="container my-5">
        <h2>Update</h2>
        <hr />
        <Form onSubmit={submitActionHandler}>
          <div className="name mb-3">
            <Form.Label>
              <strong>Image</strong>
            </Form.Label>
            <InputGroup className="d-flex gap-3">
              <Form.Control
                placeholder="Image"
                type="file"
                onChange={(e) => setImage(e.target.files[0])}
              />
            </InputGroup>
          </div>

          <div className="place-of birth mb-3">
            <Form.Label>
              <strong className="name">Nama Product</strong>
            </Form.Label>
            <InputGroup className="d-flex gap-3">
              <Form.Control
                placeholder="Nama Product"
                value={nama}
                onChange={(e) => setNama(e.target.value)}
              />
            </InputGroup>
          </div>

          <div className="birth-date mb-3">
            <Form.Label>
              <strong className="name">Deskripsi</strong>
            </Form.Label>
            <InputGroup className="d-flex gap-3">
              <Form.Control
                placeholder="Deskripsi"
                value={deskripsi}
                onChange={(e) => setDeskripsi(e.target.value)}
              />
            </InputGroup>
          </div>

          <div className="birth-date mb-3">
            <Form.Label>
              <strong className="name">Harga</strong>
            </Form.Label>
            <InputGroup className="d-flex gap-3">
              <Form.Control
                placeholder="Harga"
                value={harga}
                onChange={(e) => setHarga(e.target.value)}
              />
            </InputGroup>
          </div>
          <div>
            <div className="d-flex justify-content-end align-items-center mt-2 mb-">
              <button className="button btn btn-primary" type="submit">
                Save
              </button>
            </div>
          </div>
        </Form>
      </div>
    </div>
  );
};

export default Edit;
