import axios from "axios";
import React, { useEffect, useState } from "react";
import { Button } from "react-bootstrap";
import { useHistory } from "react-router-dom";
import Swal from "sweetalert2";
import "../style/cart.css";
import ReactPaginate from "react-paginate";

export default function Cart() {
  const [menu, setMenu] = useState([]); //useState berfungsi untuk menyimpan data sementara
  // const total = menu.reduce((a,b) => a + b.harga,0);
  const [totalHarga, setTotalHarga] = useState(0);
  const [pages, setPages] = useState(0);

  const history = useHistory();
  // const getAll = async (page = 0) => {
  //  await axios
  //     .get(`http://localhost:5000/cart/all?login_user_id=8&page=${page}` )
  //     .then((res) => {
  //       setMenu(res.data.data.content);
  //     })
  //     .catch((error) => {
  //       alert("Terjadi kesalahan" + error);
  //     });
  // };

  const getAll = async (page = 0) => {
    const res = await axios.get(
      `http://localhost:5000/cart/all?login_user_id=${localStorage.getItem(
        "id"
      )}&page=${page}`
    );
    setMenu(
      res.data.data.content.map((item) => ({
        ...item,
        checked: false,
      }))
    );
    let total = 0;
    res.data.data.content.forEach((item) => {
      total += item.totalHarga;
    });
    setTotalHarga(total);
    setPages(res.data.data.totalPages);
  };
  const checkout = async () => {
    await axios.delete(
      `http://localhost:5000/cart/checkout?loginUserId=${localStorage.getItem(
        "id"
      )}`
    );
    Swal.fire({
      icon: "success",
      title: "berhasil checkout",
      showConfirmButton: false,
      timer: 1500,
    });
    getAll();
  };

  // const remove = (idx) => {
  //   setTimeout(()=> {
  //     setListData([...totalData.slice(0, idx), ...totalData.slice(idx + 1)]);
  //   },100 );
  // };

  const konversionRupiah = (angka) => {
    return new Intl.NumberFormat("id-ID", {
      style: "currency",
      currency: "IDR",
    }).format(angka);
  };

  const deleteUser = async (id) => {
    Swal.fire({
      title: "Yakin ingin menghapus data ini?",
      text: "You won't be able to revert this!",
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Yes, delete it!",
    }).then(async (result) => {
      if (result.isConfirmed) {
        axios.delete(`http://localhost:5000/cart/` + id);
        Swal.fire("Deleted!", "Your file has been deleted.", "success");
        setTimeout(() => {
          window.location.reload();
        }, 1000);
      }
    }); // untuk pemberitahuan jika sudah berhasil menghapus
    getAll();
  };

  //   const checkout = async () => {
  //  const confirm =  await axios
  //     .get(`http://localhost:5000/cart/all?login_user_id8&page=`)
  //     .then(() => {
  //       Swal.fire({
  //         icon: "success",
  //         title: "berhasil checkout",
  //         showConfirmButton: false,
  //         timer: 1500,
  //       });
  //       if (!confirm.isConfirmed) {
  //         Swal.fire('Saved!', '', 'success')
  //       } else if (confirm.isDenied) {
  //         Swal.fire('Changes are not saved', '', 'info')
  //       }
  //     })
  //     .catch((error) => {
  //       alert("Terjadi kesalahan" + error);
  //     });
  // };

  history.push("/cart");
  useEffect(() => {
    //mengambil data, memperbarui DOM secara langsung,
    getAll();
  }, []);
  return (
    <div>
      <div className="my-5 responsive-3">
        <table className="table table-bordered">
          <thead>
            <tr className="tabel">
              <th>No</th>
              <th className="img">Image</th>
              <th className="nama">Nama Product</th>
              <th className="des">Deskripsi</th>
              <th className="jumlah">Jumlah</th>
              <th className="harga">Harga</th>
              {localStorage.getItem("id") !== null ? <th>action</th> : <></>}
            </tr>
          </thead>
          <tbody className="map">
            {menu.map(
              (
                makan,
                idx //map untuk memetakan data
              ) => (
                <tr key={makan.id}>
                  <td>{idx + 1}</td>
                  <td>
                    <img src={makan.productId.image} alt="" />
                  </td>
                  <td>{makan.productId.nama}</td>
                  <td>{makan.productId.deskripsi}</td>
                  <td>{makan.quantity}</td>
                  <td>{makan.productId.harga}</td>
                  {localStorage.getItem("id") !== null ? (
                    <td className="data">
                      <Button //button klik untuk delete
                        variant="danger"
                        className="mx-1"
                        onClick={() => deleteUser(makan.id)}
                      >
                        Hapus
                      </Button>
                      {/* untuk mengarahkan web ke path edit */}
                    </td>
                  ) : (
                    <></>
                  )}
                </tr>
              )
            )}
          </tbody>
        </table>
        <strong>Total Harga :{konversionRupiah(totalHarga)}</strong>
        <div className="checkout">
          <button onClick={() => checkout()}>Checkout</button>
        </div>
      </div>
      <div>
        <ReactPaginate
          previousLabel={"previous"}
          nextLabel={"next"}
          breakLabel={"..."}
          pageCount={pages}
          marginPagesDisplayed={2}
          pageRangeDisplayed={2}
          onPageChange={(e) => getAll(e.selected)}
          containerClassName={"pagination d-flex justify-content-center"}
          pageClassName={"page-item"}
          pageLinkClassName={"page-link"}
          previousClassName={"page-item"}
          previousLinkClassName={"page-link"}
          nextClassName={"page-item"}
          nextLinkClassName={"page-link"}
          breakClassName={"page-item"}
          breakLinkClassName={"page-link"}
          activeClassName={"active"}
        />
      </div>
      <a className="back" href="/">
        <i class="fa-solid fa-backward"></i>
      </a>
    </div>
  );
}
