// import axios from "axios";
// import React, { useEffect, useState } from "react";
// import { useHistory } from "react-router-dom";
// import Swal from "sweetalert2";
// import "../style/makanan.css";

// export default function Makanan() {
//   const [menu, setMenu] = useState([]); //useState berfungsi untuk menyimpan data sementara

//   const history = useHistory();

//   //untuk melihat semua data
//   // const getAll = () => {
//   //   axios
//   //     .get("http://localhost:5000/product")
//   //     .then((res) => {
//   //       setMenu(res.data.data);
//   //     })
//   //     .catch((error) => {
//   //       alert("Terjadi kesalahan" + error);
//   //     });
//   // };

//   const getAll = async (idx) => {
//     await axios
//       .get("http://localhost:5000/product/all?page=" + idx)
//       .then((res) => {
//         setMenu(
//           res.data.data.content.map((item) => ({
//             ...item,
//             quantity: 1,
//           }))
//         );
//       })
//       .catch((error) => {
//         alert("Terjadi kesalahan" + error);
//       });
//   };

//   const plusQuantity = (idx) => {
//     const product = [...menu];
//     if (product[idx].quantity <= 44) {
//       product[idx].quantity++;
//       setMenu(product);
//     }
//   };

//   const minesQuantity = (idx) => {
//     const product = [...menu];
//     if (product[idx].quantity > 1) {
//       product[idx].quantity--;
//       setMenu(product);
//     }
//   };

//   //   const addToCart = async (makanan) => {
//   //     await axios
//   //     .post("http://localhost:5000/cart/", makanan)
//   //     .then(() => {
//   //       Swal.fire({
//   //         icon: "success",
//   //         title: "berhasil masuk ke keranjang",
//   //         showConfirmButton: false,
//   //         timer: 1500,
//   //       });
//   //       history.push("/cart")
//   //       window.location.reload();
//   //     })
//   //     .catch((error) => {
//   //       alert("Terjadi kesalahan" + error);
//   //     });
//   // };

//   const addToCart = (makanan) => {
//     console.log(makanan);
//     axios
//       .post("http://localhost:5000/cart/", {
//         loginUserId: localStorage.getItem("id"),
//         productId: makanan.id,
//         quantity: makanan.quantity,
//       })
//       .then(() => {
//         Swal.fire({
//           icon: "success",
//           title: "berhasil masuk ke keranjang",
//           showConfirmButton: false,
//           timer: 1500,
//         });
//       })
//       .catch((error) => {
//         alert("Terjadi kesalahan" + error);
//       });
//   };

//   useEffect(() => {
//     //mengambil data, memperbarui DOM secara langsung,
//     getAll();
//   }, []);
//   return (
//     <div className="mkn">
//       <div className="cr flex-wrap">
//         {menu.map((makanan, idx) => (
//           <div
//             data-aos="zoom-in"
//             className="card mb-3"
//             style={{ width: "18rem" }}
//           >
//             <img src={makanan.image} className="card-img-top" alt="..." />
//             <div className="card-body">
//               <h5 className="card-title">{makanan.nama}</h5>
//               <p className="card-text">{makanan.deskripsi}</p>
//               <p>
//                 {" "}
//                 {makanan.quantity == 1 ? (
//                   <button
//                     className="qty gray"
//                     onClick={() => minesQuantity(idx)}
//                   >
//                     {" "}
//                     -{" "}
//                   </button>
//                 ) : (
//                   <button
//                     className="qty green"
//                     onClick={() => minesQuantity(idx)}
//                   >
//                     {" "}
//                     -{" "}
//                   </button>
//                 )}
//                 {makanan.quantity}
//                 {makanan.quantity <= 44 ? (
//                   <button
//                     className="qty green"
//                     onClick={() => plusQuantity(idx)}
//                   >
//                     +
//                   </button>
//                 ) : (
//                   <button
//                     className="qty gray"
//                     onClick={() => plusQuantity(idx)}
//                   >
//                     +
//                   </button>
//                 )}
//               </p>
//               <p>Rp.{makanan.harga}</p>
//               {localStorage.getItem("id") !== null ? (
//                 <a
//                   onClick={() => addToCart(makanan)}
//                   className="btn btn-warning"
//                 >
//                   Beli
//                 </a>
//               ) : (
//                 <></>
//               )}
//             </div>
//           </div>
//         ))}
//       </div>
//     </div>
//   );
// }
