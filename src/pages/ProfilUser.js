import axios from "axios";
import React, { useEffect, useState } from "react";
import { Form, InputGroup, Modal } from "react-bootstrap";
import Swal from "sweetalert2";
import "../style/profil.css";
import Image from "../Image/waroeng.jpeg";


export default function ProfilUser() {
  const [profile, setProfile] = useState({
    fotoProfile: null,
    nama: "",
    alamat: "",
    email: "",
    noTlpn: "",
    password: "",
  });
  const [email, setEmail] = useState("");
  const [fotoProfile, setFotoProfile] = useState(null);
  const [nama, setNama] = useState("");
  const [alamat, setAlamat] = useState("");
  const [noTlpn, setNoTlpn] = useState("");
  const [password, setPassword] = useState("");
  const [show, setShow] = useState(false);
  const handleClose = () => setShow(false); // fungsi handleClose akan menyetel variabel acara ke false.
  const handleShow = () => setShow(true); //  menyetel variabel status acara ke true,

  const getAll = async () => {
    await axios
      .get("http://localhost:5000/user/" + localStorage.getItem("id"))
      .then((res) => {
        setProfile(res.data.data);
      })
      .catch((error) => {
        alert("Terjadi kesalahan" + error);
      });
  };

  useEffect(() => {
    //mengambil data, memperbarui DOM secara langsung,
    getAll(0);
  }, []);

  useEffect(() => {
    //mengambil data, memperbarui DOM secara langsung,
    axios
      .get("http://localhost:5000/user/" + localStorage.getItem("id"))
      .then((response) => {
        const profil = response.data.data;
        setFotoProfile(profil.fotoProfile);
        setNama(profil.nama);
        setAlamat(profil.alamat);
        setNoTlpn(profil.noTlpn);
        setEmail(profil.email);
        setPassword(profil.password);
      })
      .catch((error) => {
        alert("Terjadi kesalahan Sir!!" + error);
      });
  }, []);

  // const putProfile = async (e) => {
  //   // e.preventDefault();

  //   await Swal.fire({
  //     title: "Apakah Anda Yakin Untuk Di Edit?",
  //     icon: "warning",
  //     showCancelButton: true,
  //     confirmButtonColor: "#3085d6",
  //     cancelButtonColor: "#d33",
  //     confirmButtonText: "Yes!",
  //   })
  //     .then((res) => {
  //       if (res.isConfirmed)
  //         axios.put(
  //           "http://localhost:5000/user/" + localStorage.getItem("id"),
  //           formData
  //         );
  //       Swal.fire({
  //         title: "Berhasil",
  //         icon: "success",
  //         showConfirmButton: false,
  //         timer: 1500,
  //       });
  //       setTimeout(() => {
  //         // window.location.reload();
  //       }, 1000);
  //     })
  //     .catch((error) => {
  //       alert("Terjadi Kesalahan " + error);
  //       console.log(error);
  //     });
  //   getAll();
  // };

  const putProfile = async (e) => {
    e.preventDefault();
    e.persist();

    const formData = new FormData();
    formData.append("file", fotoProfile);
    formData.append("nama", nama);
    formData.append("alamat", alamat);
    formData.append("noTlpn", noTlpn);
    formData.append("email", email);
    formData.append("password", password);

    try {
      // library opensource yg digunkan untuk request data melalui http
      await axios.put(
        `http://localhost:5000/user/${localStorage.getItem("id")}`,
        formData
        // headers berikut berfungs untuk method yg hanya diakses oleh admin
      );
      // Sweet alert
      setShow(false);
      Swal.fire({
        icon: "success",
        title: "Berhasil mengedit data!!",
        showConfirmButton: false,
        timer: 1500,
      });
      setTimeout(() => {
      }, 1500);
    } catch (error) {
      console.log(error);
    }
  };


  return (
    <div >
        <h2 className="akn">Profil Saya</h2>
        <p className="akn">
          Kelola informasi profil Anda untuk mengontrol, melindungi dan
          mengamankan akun
        </p>
        {/* <Form onSubmit={addUser}> */}
        {/* <hr /> */}
      <div className="ya">
        <div className="wee">
          <div>
            <img
              className="ye"
              src={profile.fotoProfile}
              alt=""
              width={300}
              height={250}
              type="file"
            />
          </div>
          <div className="in">
            <div className="d-flex ">
              {/* <strong>Username : </strong> */}
              <h2>{profile.nama}</h2>
            </div>
            <div className="d-flex ">
              {/* <strong>Email : </strong> */}
              <p><i class="fa-solid fa-envelope"></i> {profile.email}</p>
            </div>
            <div className="d-flex ">
              {/* <strong>Alamat : </strong> */}
            <p><i class="fa-solid fa-location-dot"></i> {profile.alamat}</p>
            </div>
            <div className="d-flex">
              {/* <strong>No telpon : </strong> */}
           <p><i class="fa-solid fa-phone-volume"></i> {profile.noTlpn}</p>
            </div>
            {/* <div className="mb-3 ">
              <Form.Label>
                <strong>Password</strong>
              </Form.Label>
              <InputGroup className="nama">
                <Form.Control
                  type="password"
                  placeholder="masukkan nomor"
                  value={profile.password}
                  required
                />
              </InputGroup>
            </div> */}
            <button onClick={handleShow} className="sim" type="button">
              edit
            </button>
          </div>
        </div>
        {/* </Form> */}
        <div className="hh">
        <img className="iii" src={Image} width={250} height={200} />
        <h2><i class="fa-solid fa-shop"></i> WAROENG ALFATHAR</h2>
        <h2>waroeng belanja terpercaya</h2>
        </div>
      </div>
      <Modal show={show} onHide={handleClose}>
        <Modal.Header
          className="add"
          closeButton
          style={{ backgroundColor: "#2B3A55", color: "white" }}
        >
          <Modal.Title>edit data</Modal.Title>
        </Modal.Header>
        <Modal.Body style={{ backgroundColor: "#FFF8BC" }}>
          <Form onSubmit={putProfile}>
            <div className="mb-3">
              <Form.Label>
                <strong>Foto profil</strong>
              </Form.Label>
              <InputGroup className="d-flex gap-3">
                <Form.Control
                  placeholder="Masukkan Link"
                  // value={image}
                  type="file"
                  onChange={(e) => setFotoProfile(e.target.files[0])}
                  required
                />
              </InputGroup>
            </div>
            <div className="mb-3">
              <Form.Label>
                <strong>Email</strong>
              </Form.Label>
              <InputGroup className="d-flex gap-3">
                <Form.Control
                  placeholder="Masukkan email"
                  value={email}
                  onChange={(e) => setEmail(e.target.value)}
                  required
                />
              </InputGroup>
            </div>
            <div className="mb-3">
              <Form.Label>
                <strong>Nama</strong>
              </Form.Label>
              <InputGroup className="d-flex gap-3">
                <Form.Control
                  placeholder="Masukkan nama "
                  value={nama}
                  onChange={(e) => setNama(e.target.value)}
                  required
                />
              </InputGroup>
            </div>
            <div className="mb-3">
              <Form.Label>
                <strong>alamat</strong>
              </Form.Label>
              <InputGroup className="d-flex gap-3">
                <Form.Control
                  type="text"
                  placeholder="Masukkan alamat"
                  value={alamat}
                  onChange={(e) => setAlamat(e.target.value)}
                  required
                />
              </InputGroup>
            </div>
            <div className="mb-3">
              <Form.Label>
                <strong>NoTlpn</strong>
              </Form.Label>
              <InputGroup className="d-flex gap-3">
                <Form.Control
                  placeholder="Masukkan Nomer Telepon"
                  value={noTlpn}
                  type={Number}
                  onChange={(e) => setNoTlpn(e.target.value)}
                  required
                />
              </InputGroup>
            </div>
            <div className="mb-3">
              <Form.Label>
                <strong>Password</strong>
              </Form.Label>
              <InputGroup className="d-flex gap-3">
                <Form.Control
                  placeholder="Masukkan Nomer Telepon"
                  value={password}
                  type="password"
                  onChange={(e) => setPassword(e.target.value)}
                  required
                />
              </InputGroup>
            </div>
            <button
              className="mx-1 button-btl btn btn-danger"
              onClick={handleClose}
            >
              Close
            </button>
            <button
              type="submit"
              className="mx-1 button btn btn-primary"
              onClick={handleClose}
            >
              Save
            </button>
          </Form>
        </Modal.Body>
      </Modal>
    </div>
  );
}
