// import axios from "axios";
import axios from "axios";
import React, { useState } from "react";
import { Form, InputGroup } from "react-bootstrap";
import { useHistory } from "react-router-dom";
import Swal from "sweetalert2";
import "../style/login.css"

export default function Register() {
  const [email, setEmail] = useState("");
  const [fotoProfile, setFotoProfile] = useState(null);
  const [nama, setNama] = useState("");
  const [alamat, setAlamat] = useState("");
  const [noTlpn, setNoTlpn] = useState("");
  const [password, setPassword] = useState("");
  const [role, setRole] = useState("");
  const [show, setShow] = useState(false);
  const history = useHistory();

  // const [userRegister,setUserRegister] = useState({
  //   email: '',
  //   password: '',
  //   role: 'USER',
  // })

  // const handleOnChange = (e) => {
  //   setUserRegister((currUser) => {
  //     return { ...currUser,[e.target.id] : e.target.value}
  //   })
  // }

  // const register = async (e) => {
  //   e.preventDefault()
  //   try{
  //     const response = await fetch('http://localhost:5000/user/all', {
  //       method: 'POST',
  //       headers: {
  //         'Content-Type': 'application/json',
  //       },
  //       body: JSON.stringify(userRegister),
  //     })

  //     if (response.ok) {
  //       alert('register success')
  //       setTimeout(() => {
  //         history.push('/login')
  //       },1250)
  //     }
  //   }catch(err) {
  //     console.log(err)
  //   }
  // }

  const register = async (e) => {
    e.preventDefault();
    e.persist();

    const formData = new FormData();
    formData.append("file", fotoProfile);
    formData.append("nama", nama);
    formData.append("alamat", alamat);
    formData.append("noTlpn", noTlpn);
    formData.append("email", email);
    formData.append("password", password);
    formData.append("role", "USER");

    // try catch untuk memastikan trjdi kesalahan
    try {
      // library opensource yg digunkan untuk request data melalui http
      await axios.post(
        "http://localhost:5000/user",
        formData
        // headers berikut berfungs untuk method yg hanya diakses oleh admin
      );
      // Sweet alert
      setShow(false);
      Swal.fire({
        icon: "success",
        title: "Berhasil Register!!",
        showConfirmButton: false,
        timer: 1500,
      });
      setTimeout(() => {
        history.push("/login");
      }, 1500);
    } catch (error) {
      console.log(error);
    }
  };

  // const [email, setEmail] = useState("");
  // const [password, setPassword] = useState("");

  // const history = useHistory();

  // const data = {
  //   email: email,
  //   password: password,
  // };
  // const register = async (e) => {
  //   e.preventDefault();
  //   axios.post("http://localhost:8000/akuns", data);
  //   Swal.fire({
  //     icon: "success",
  //     title: "Your work has been saved",
  //     showConfirmButton: false,
  //     timer: 1500,
  //   })
  //     .then(() => {
  //       window.location.reload(); //otomatis reload web setelah menambahkan data
  //     })
  //     .catch((error) => {
  //       // alert error untuk mengetahui jika terjadi error
  //       alert("Terjadi kesalahan" + error);
  //     });
  //   localStorage.setItem("id", data.id);
  //   history.push("/");
  // };

  return (
    <div className="container border my-5 pt-3 pb-5 px-5">
      <div className="srr">
        <img
          src="https://www.pngitem.com/pimgs/m/237-2370224_register-user-register-icon-png-transparent-png.png"
          alt=""
          width={200}
        />
      </div>
      <h1 className="mb-5">Form Register</h1>
      <Form onSubmit={register} method="POST">
        <div className="mb-3">
          <Form.Label>
            <strong>Email</strong>
          </Form.Label>
          <InputGroup className="d-flex gap 3">
            <Form.Control
              id="email"
              placeholder="Email address"
              type="email"
              value={email}
              onChange={(e) => setEmail(e.target.value)}
              required
            />
          </InputGroup>
        </div>
        <div className="mb-3">
          <Form.Label>
            <strong>Password</strong>
          </Form.Label>
          <InputGroup className="d-flex gap 3">
            <Form.Control
              id="password"
              placeholder="Password"
              type="password"
              value={password}
              onChange={(e) => setPassword(e.target.value)}
              required
            />
          </InputGroup>
        </div>
        <div className="mb-3">
          <Form.Label>
            <strong>Foto Profile</strong>
          </Form.Label>
          <InputGroup className="d-flex gap 3">
            <Form.Control
              id="password"
              placeholder="Password"
              type="file"
              // value={userRegister.password}
              onChange={(e) => setFotoProfile(e.target.files[0])}
              required
            />
          </InputGroup>
        </div>
        <div className="mb-3">
          <Form.Label>
            <strong>Username</strong>
          </Form.Label>
          <InputGroup className="d-flex gap 3">
            <Form.Control
              id="username"
              placeholder="username"
              type="username"
              value={nama}
              onChange={(e) => setNama(e.target.value)}
              // value={userRegister.email}
              required
            />
          </InputGroup>
        </div>
        <div className="mb-3">
          <Form.Label>
            <strong>Alamat</strong>
          </Form.Label>
          <InputGroup className="d-flex gap 3">
            <Form.Control
              id="alamat"
              placeholder="masukkan alamat"
              type="alamat"
              value={alamat}
              onChange={(e) => setAlamat(e.target.value)}
              // value={userRegister.email}
              required
            />
          </InputGroup>
        </div>
        <div className="mb-3">
          <Form.Label>
            <strong>No telpon</strong>
          </Form.Label>
          <InputGroup className="d-flex gap 3">
            <Form.Control
              id="alamat"
              placeholder="masukkan alamat"
              type="alamat"
              value={noTlpn}
              onChange={(e) => setNoTlpn(e.target.value)}
              // value={userRegister.email}
              required
            />
          </InputGroup>
        </div>
        <button type="submit" className="re btn">
          Register
        </button>
      </Form>
      <p class="regis">
        <h2 className="notReg">Silahkan Login Jika sudah Memiliki Akun</h2>
        <a href="/login" className="mx-1  btn btn-primary">
          {" "}
          Login
        </a>
      </p>
    </div>
  );
}
